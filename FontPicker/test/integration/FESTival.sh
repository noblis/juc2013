#!/bin/bash

# 	Copyright (c) 2013, Noblis
# 	All rights reserved.
#
# 	Redistribution and use in source and binary forms, with or without modification, 
# 	are permitted provided that the following conditions are met:
#
#		Redistributions of source code must retain the above copyright notice, 
#		this list of conditions and the following disclaimer.
#
#		Redistributions in binary form must reproduce the above copyright notice, 
#		this list of conditions and the following disclaimer in the documentation and/or 
#		other materials provided with the distribution.

#		Neither the name of Noblis nor the names of its contributors may be used to 
#		endorse or promote products derived from this software without specific prior 
#		written permission.
#
#	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
#	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
#	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
#	IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
#	INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
#	BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
#	OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
#	WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
#	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
#	OF SUCH DAMAGE.i


set -e

trap 'exit 1' INT

set -x
 
FEST_CLASS=$1
PROJECT=$2
 
if [ -z "$FEST_CLASS" ]; then
   echo error: missing FEST_CLASS parameter
   exit 1
fi

if [ -z "$PROJECT" ]; then
   echo error: missing PROJECT parameter
   exit 1
fi 

if [ -z "$WORKSPACE" ]; then
   echo error: missing WORKSPACE environment variable
   exit 1
fi

 
if [ -z "$BUILD_NUMBER" ]; then
   echo error: missing BUILD_NUMBER environment variable
   exit 1
fi
 

if [ -z "$DISPLAY" ]; then
   echo error: missing DISPLAY environment variable
   exit 1
fi

FLV_DISPLAY=`expr $DISPLAY`

# go to vnc2flv directory
cd /var/lib/jenkins/vnc2flv-20100207/tools/
 
# remove previously recorded video, if one exists
rm -rf fest.flv

# record start timestamp
echo -n "Start: " > $WORKSPACE/$PROJECT/fest_timestamps.txt && date +%Y%m%d%H%M%S | cat >> $WORKSPACE/$PROJECT/fest_timestamps.txt

# start recording
# NOTE: currently flvrec.py complains when attempting to use the $DISPLAY parameter passed in by Jenkins, it seems to
# want an int and thinks the parameter is a string

python flvrec.py -K 1 -r 1 -P /var/lib/jenkins/.vnc/passwd -o fest.flv "localhost:$FLV_DISPLAY" &

# get the process id of the recording
FLVREC_PID=$(echo $!)
echo FLVREC_PID: $FLVREC_PID

# move to the directory where TestRunner.groovy is located
cd $WORKSPACE/$PROJECT/test/integration

# set up the java opts for all the FESTivities, adjust accordingly based on application-specific needs 
#export JAVA_OPTS="-Xmx2048m -XX:PermSize=1024M -XX:MaxPermSize=2048m"

# run the fest tests
groovy GriffonFestRunner.groovy -o $FEST_CLASS

 
# send the SIGINT to stop the recording process
/bin/kill -SIGINT $FLVREC_PID

# split the video for each test method, and remove the boring parts
groovy VideoEdit.groovy /var/lib/jenkins/vnc2flv-20100207/tools/fest.flv $WORKSPACE/$PROJECT/fest_timestamps.txt $FEST_CLASS-$BUILD_NUMBER-


# optionally copy videos somewhere, you could always use a plugin (i.e. SSH Publisher) for this as well

if [ -n "${COPY_VIDEOS}" ];
then
  ssh $USER@$DESTINATION_MACHINE mkdir -p -v $DESTINATION_DIR
  scp -r "$WORKSPACE"/$PROJECT/target/fest_runner-reports/temp $USER@$DESTINATION_MACHINE:$DESTINATION_DIR/$FEST_CLASS
  videos=($WORKSPACE/$PROJECT/test/integration/$FEST_CLASS-$BUILD_NUMBER*.flv)

  if [ -e ${videos[0]} ];
  then
    #If there are video files, copy them to $DESTINATION_MACHINE/$DESTINATION_DIR
    ssh $USER@$DESTINATION_MACHINE "mkdir -p $DESTINATION_DIR/$FEST_CLASS"
    scp $WORKSPACE/$PROJECT/test/integration/$FEST_CLASS-$BUILD_NUMBER*.flv $USER@$DESTINATION_MACHINE:$DESTINATION_DIR/$FEST_CLASS
  else
    echo "No videos found to archive..."
  fi
fi
