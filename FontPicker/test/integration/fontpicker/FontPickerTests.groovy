package fontpicker

import org.fest.swing.fixture.*
import org.fest.swing.timing.Pause
import griffon.fest.FestSwingTestCase
import java.awt.Font
import java.awt.GraphicsEnvironment
import java.awt.event.KeyEvent
import javax.swing.JSlider
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class FontPickerTests extends FestSwingTestCase {
    // instance variables:
    // app    - current application
    // window - value returned from initWindow()
    //          defaults to app.windowManager.windows[0]

	private static final Logger logger = LoggerFactory.getLogger(FontPickerTests.class)
	private static final String panelUpdateFail = 'Font pane did not properly update all font examples after settings change'
 
	void testSizeUpdate(){
		
		JSlider slider = window.slider('fontSize').component()
		
		[slider.getMinimum(), (int) (slider.getMaximum()/2), slider.getMaximum()].each{
	
			logger.debug("Checking display updates after changing font size to ${it}.")
			window.slider('fontSize').slideTo(it)
			window.button('btnRefresh').click()
			checkPanelsAfterUpdate()
			Pause.pause(2000)
		}
	}

	void testStyleUpdate(){
		JToggleButtonFixture bold = window.toggleButton('btnBold')
		JToggleButtonFixture italicize = window.toggleButton('btnItalic')
		
		//Bold
		logger.debug("Checking display updates after only bolding.")
		bold.click()
		bold.requireSelected()
		checkPanelsAfterUpdate()
		Pause.pause(2000)
		
		//Italicize
		logger.debug("Checking display updates after bolding and italicizing.")
		italicize.click()
		italicize.requireSelected()
		checkPanelsAfterUpdate()
		Pause.pause(2000)
		
		//Un-bold
		logger.debug("Checking display updates after un-bolding (only italicized).")
		bold.click()
		bold.requireNotSelected()
		checkPanelsAfterUpdate()
		Pause.pause(2000)
		
		//Un-italicize
		logger.debug("Checking display updates after un-bolding and un-italicizing.")
		italicize.click()
		italicize.requireNotSelected()
		checkPanelsAfterUpdate()
		Pause.pause(2000)
	}
	
	void testSampleTextUpdate() {
		
		['test','', 'test2'].each{ 
			
			logger.debug("Checking display updates after changing sample text to: ${it}")
        	        
			if(window.textBox('textField').text()) {
				window.textBox('textField').setText('')
			}
			
			window.textBox('textField').enterText(it)
			window.textBox('textField').pressAndReleaseKeys(KeyEvent.VK_TAB)
			checkPanelsAfterUpdate()
			Pause.pause(2000)			
		}
    }

 	void checkPanelsAfterUpdate(){
		def ge = GraphicsEnvironment.getLocalGraphicsEnvironment()
        def fonts = ge.getAllFonts()
		
		//Check that the correct number of examples are displayed after update
		int numFontPanels = window.panel('pane').component().getComponents().size()
		if (numFontPanels != fonts.size()){
			fail("${panelUpdateFail}.  All fonts were not displayed.")
		}
			
		//Check that all examples were updated based on current text
		
		String displayText = window.textBox('textField').component().text
		boolean shouldBeBolded = window.toggleButton('btnBold').component().isSelected()
		boolean shouldBeItalicized = window.toggleButton('btnItalic').component().isSelected()
		int fontSize = window.slider('fontSize').component().getValue()
		
		window.panel('pane').component().getComponents().each(){
			
			//Check that text is correct
			JLabelFixture label = new JLabelFixture(window.robot, it.getComponent(0))
			label.requireText(displayText)
			
			//Check that font style is correct
			Font font = label.component().getFont()
						
			if (shouldBeBolded && !font.isBold()){ 
				fail("${panelUpdateFail}.  All fonts were not appropriately bolded.")
			}
			else if(shouldBeItalicized && !font.isItalic()){
				fail("${panelUpdateFail}.  All fonts were not appropriately italicized.")
			}
			
			//Check that the font size is correct
			if (font.getSize() != fontSize){
				fail("${panelUpdateFail}.  All fonts were not appropriately sized.")
			}
		}
		
		Pause.pause(1500)
		
	}
}
