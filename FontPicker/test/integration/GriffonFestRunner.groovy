// 	Copyright (c) 2013, Noblis
//	All rights reserved.
//
// 	Redistribution and use in source and binary forms, with or without modification, 
// 	are permitted provided that the following conditions are met:
//
//		Redistributions of source code must retain the above copyright notice, 
//		this list of conditions and the following disclaimer.
//
//		Redistributions in binary form must reproduce the above copyright notice, 
//		this list of conditions and the following disclaimer in the documentation and/or 
//		other materials provided with the distribution.

//		Neither the name of Noblis nor the names of its contributors may be used to 
//		endorse or promote products derived from this software without specific prior 
//		written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
//	IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
//	INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
//	BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
//	OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
//	WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
//	OF SUCH DAMAGE.

import java.text.SimpleDateFormat
import java.util.regex.Matcher

public class GriffonFestRunner {
    private static void printResults(TestClass testClass) {
        println '-' * 80
        println "Results for ${testClass.className}"
        testClass.testMethods.each { TestClass.TestMethod testMethod ->
            println '\t' + testMethod.resultText()
        }
        println()
        println "\tTests passed: ${testClass.passedCount()}"
        println "\tTests failed: ${testClass.failedCount()}"
        println '-' * 80
        println()
    }

    private static void createAntFile() {
        new File(TestClass.testReportDestDir, 'build.xml').write("""<?xml version="1.0"?>
<project name="fest_runner">
    <target name="create-unit-test-report"
            description="Generate reports for executed JUnit unit tests.">

        <delete quiet="true" includeemptydirs="true">
            <fileset dir="../temp" defaultexcludes="false" />
        </delete>

        <mkdir dir="../temp" />

        <junitreport todir="./">
            <fileset dir="./">
                <include name="TEST-*.xml"/>
            </fileset>
            <report format="frames" todir="./"/>
        </junitreport>

        <copy todir="../temp">
            <fileset dir="./" excludes="build.xml" />
        </copy>
    </target>
</project>""")
    }

    private static void generateHtmlReport() {
        createAntFile()
        Process process = 'ant create-unit-test-report'.execute((List) null, new File(TestClass.testReportDestDir))
        process.consumeProcessOutput(System.out, System.err)
        process.waitFor()
        new File(TestClass.testReportDestDir, 'build.xml').delete()
    }

    public static void main(String[] args) {
        CliBuilder cli = new CliBuilder(usage: 'groovy GriffonFestRunner.groovy -[h] -[o] -[x] <X11DISPLAY> <TEST_CLASS [TEST_CLASS ...]>')
        cli.with {
            h longOpt: 'help', 'Show usage information'
            o longOpt: 'output', 'Print test output'
            x args: 1, argName: 'x11Display', 'X11 display number to use when executing the tests, defaults to the value of $DISPLAY env variable'
        }

        OptionAccessor options = cli.parse(args)
        if(options.h || !options?.arguments()) {
            cli.usage()
            return
        }

        String x11Display = options.x ?: System.getenv('DISPLAY')

        int passedTests = 0
        int failedTests = 0
        int totalTests = 0

        List<String> wholeClasses = []
        Map<String, List<String>> partialClasses = [:]
        options.arguments().each { String arg ->
            if(arg.count('.') == 0) {
                wholeClasses << arg
                //Remove all references to this class from partialClasses
                partialClasses = partialClasses.findAll { !wholeClasses.contains(it.key) }
            } else if(arg.count('.') == 1) {
                String className = arg.tokenize('.')[0]
                String methodName = arg.tokenize('.')[1]
                if(!wholeClasses.contains(className)) {
                    //Only add this test method if the class isn't already in wholeClasses
                    partialClasses[className] = partialClasses.get(className, []) + methodName
                }
            } else {
                throw new Exception("Invalid test name: ${arg}")
            }
        }

        List<TestClass> testClasses = []
        testClasses += wholeClasses.collect { String className ->
            return new TestClass(className, x11Display, (boolean) options.o)
        }
        testClasses += partialClasses.collect { String className, List<String> testMethods ->
            return new TestClass(className, testMethods, x11Display, (boolean) options.o)
        }

        testClasses.each { TestClass testClass ->
            testClass.runTests()
            printResults(testClass)
            passedTests += testClass.passedCount()
            failedTests += testClass.failedCount()
            totalTests += testClass.passedCount() + testClass.failedCount()
        }

        if(totalTests > 0) {
            generateHtmlReport()
        }

        //This is necessary, because sometimes TestRunner hangs after main() finishes for mysterious reasons.
        System.exit(0)
    }
}

public class TestClass {
    public static final String testTime = new SimpleDateFormat('yyyyMMddHHmmssSSS').format(new Date())
    public static final String testReportDestDir = "../../target/fest_runner-reports/${testTime}"
    public static final String testReportXmlSrcDir = '../../target/test-reports'
    public static final String testReportXmlDestDir = "${testReportDestDir}/xml"

    public final String className
    public final String packageName
    public final String classFilePath
    private final boolean printOutput
    private final String x11Display
    public final List<TestMethod> testMethods = []

    public TestClass(String className, String x11Display, boolean printOutput = false) {
        this.className = className
        this.packageName = getClassPackage(className)
        this.classFilePath = makeClassFilePath(packageName, className)
        this.printOutput = printOutput
        this.x11Display = x11Display

        testMethods = extractTestMethods()
    }

    public TestClass(String className, List<String> testNames, String x11Display, boolean printOutput = false) {
        this.className = className
        this.packageName = getClassPackage(className)
        this.classFilePath = makeClassFilePath(packageName, className)
        this.printOutput = printOutput
        this.x11Display = x11Display

        //Validate existence of test methods in testNames
        List<String> testMethodsInFile = extractTestMethods()*.methodName
        testNames.each { String testName ->
            if(!testMethodsInFile.contains(testName)) {
                throw new Exception("Invalid test method name: ${testName}")
            }
        }

        testMethods = testNames.collect { String testName ->
            return new TestMethod(testName, x11Display, printOutput)
        }
    }

    private static String getClassPackage(String className) {
        List<String> foundPackages = []
        String classFileName = "${className}Tests.groovy"
        new File('./').eachDirRecurse { File dir ->
            if(classFileName in dir.list()) {
                foundPackages << dir.path.replaceFirst('[.]/', '').replace('/', '.')
            }
        }

        if(foundPackages.size() == 0) {
            throw new Exception("Invalid test class name: ${className}, class does not exist in any packages")
        }

        if(foundPackages.size() > 1) {
            throw new Exception("Invalid test class name: ${className}, class exists in multiple packages")
        }

        return foundPackages.first()
    }

    private static String makeClassFilePath(String _package, String className) {
        return new File(_package.replace('.', '/'), "${className}Tests.groovy").absolutePath
    }

    private List<TestMethod> extractTestMethods() {
        File f = new File(classFilePath)
        Matcher matcher = (f.text =~ /(?:void|def)\s+(test\w+)[(]/)

        List<TestMethod> tempMethods = matcher.collect {
            return new TestMethod(it[1].toString(), x11Display, printOutput)
        }

        tempMethods[0] = new TestMethod(matcher[0][1].toString(), x11Display, printOutput)

        return tempMethods
    }

    public void runTests() {
        println "Running ${testMethods.size()} tests for class: ${className}"
        testMethods*.runTest()
        consolidateXmlReports()
    }

    private void consolidateXmlReports() {
        if(!testMethods) {
            //Skip report consolidation if there are no reports
            return
        }

        List<Node> xmlReports = testMethods.collect { TestMethod testMethod ->
            return new XmlParser().parseText(new File(testMethod.xmlReportDestPath).text)
        }

        Node combinedReport = xmlReports.first()

        double time = Double.parseDouble(combinedReport.@time)
        int errors = Integer.parseInt(combinedReport.@errors)
        int failures = Integer.parseInt(combinedReport.@failures)
        int tests = Integer.parseInt(combinedReport.@tests)
        xmlReports.tail().each { Node xmlReport ->
            time += Double.parseDouble(xmlReport.@time)
            errors += Integer.parseInt(xmlReport.@errors)
            failures += Integer.parseInt(xmlReport.@failures)
            tests += Integer.parseInt(xmlReport.@tests)

            combinedReport.append(xmlReport.testcase[0])
            combinedReport.appendNode('system-out', combinedReport."system-out"[0].text() + '\n' + xmlReport."system-out"[0].text())
            combinedReport.appendNode('system-err', combinedReport."system-err"[0].text() + '\n' + xmlReport."system-err"[0].text())
            combinedReport.remove(combinedReport."system-out"[0])
            combinedReport.remove(combinedReport."system-err"[0])
        }

        combinedReport.@errors = errors.toString()
        combinedReport.@failures = failures.toString()
        combinedReport.@tests = tests.toString()
        combinedReport.@time = time.toString()

        StringWriter writer = new StringWriter()
        new XmlNodePrinter(new PrintWriter(writer)).print(combinedReport)
        new File(testReportDestDir, "TEST-integration-integration-${packageName}.${className}Tests.xml").write('<?xml version="1.0" encoding="UTF-8" ?>\n' + writer.toString())
    }

    public int passedCount() {
        return testMethods.count { TestMethod testMethod ->
            return testMethod.testPassed()
        }
    }

    public int failedCount() {
        return testMethods.count { TestMethod testMethod ->
            return !testMethod.testPassed()
        }
    }

    public class TestMethod {
        private static final File testStartDir = new File('../../')

        public final String methodName
        public final String runCommand
        private final boolean printOutput
        private final String x11Display
        public final String xmlReportSrcPath
        public final String xmlReportDestPath

        private Boolean testResult = null

        public TestMethod(String methodName, String x11Display, boolean printOutput = false) {
            this.methodName = methodName
            this.runCommand = "griffon -Dgriffon.artifact.force.upgrade=true -Dgriffon.env=festTest test-app integration:integration ${packageName}.${className}.${methodName}"
            this.printOutput = printOutput
            this.x11Display = x11Display

            this.xmlReportSrcPath = "${testReportXmlSrcDir}/TEST-integration-integration-${packageName}.${className}Tests.xml"
            this.xmlReportDestPath = "${testReportXmlDestDir}/TEST-integration-integration-${packageName}.${className}Tests-${methodName}.xml"
        }

        private List<String> getEnvVars() {
            Map<String, String> env = new HashMap<String, String>(System.getenv())
            env['DISPLAY'] = x11Display
            return env.collect { String k, String v ->
                return "$k=$v"
            }
        }

        public void runTest() {
            println "\tRunning FEST test: ${runCommand}"
            new File(testStartDir, 'fest_timestamps.txt').append("${className}.${methodName}(): ")

            Process process = runCommand.execute(getEnvVars(), testStartDir)
            if(printOutput) {
                process.consumeProcessOutput(System.out, System.err)
            } else {
                process.consumeProcessOutput()
            }
            process.waitFor()

            new File(testStartDir, 'fest_timestamps.txt').append(" ${new SimpleDateFormat('yyyyMMddHHmmss').format(new Date())}\n")

            saveTestReport()
            if(process.exitValue() == 0) {
                testResult = true
                println '\t...passed'
            } else {
                testResult = false
                println '\t...FAILED'
            }
        }

        public boolean testPassed() {
            if(testResult == null) {
                throw new Exception()
            }

            return testResult
        }

        public String resultText() {
            if(testResult == null) {
                throw new Exception()
            }

            if(testResult) {
                return "${methodName}...passed"
            } else {
                return "${methodName}...FAILED"
            }
        }

        private void saveTestReport() {
            new File(testReportXmlDestDir).mkdirs()
            new File(xmlReportDestPath).write(new File(xmlReportSrcPath).text)
        }
    }
}
