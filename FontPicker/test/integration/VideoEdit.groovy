// 	Copyright (c) 2013, Noblis
//	All rights reserved.
//
// 	Redistribution and use in source and binary forms, with or without modification, 
// 	are permitted provided that the following conditions are met:
//
//		Redistributions of source code must retain the above copyright notice, 
//		this list of conditions and the following disclaimer.
//
//		Redistributions in binary form must reproduce the above copyright notice, 
//		this list of conditions and the following disclaimer in the documentation and/or 
//		other materials provided with the distribution.

//		Neither the name of Noblis nor the names of its contributors may be used to 
//		endorse or promote products derived from this software without specific prior 
//		written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
//	IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
//	INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
//	BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
//	OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
//	WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
//	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
//	OF SUCH DAMAGE.

import groovy.time.TimeCategory
import groovy.transform.Immutable

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.regex.Matcher

class VideoEdit {
    @Immutable
    public static class VideoSegment {
        String methodName
        int startIndex, endIndex

        public String getOutputFileName(String outputFilePrefix) {
            return "${outputFilePrefix}${methodName}.flv"
        }

        public String getTrimCommand(File videoFile, String outputFilePrefix) {
            if(videoFile.exists()) {
                return "ffmpeg -ss ${startIndex} -t ${endIndex - startIndex} -i ${videoFile.canonicalPath} -acodec copy -vcodec copy ${getOutputFileName(outputFilePrefix)}"
            } else {
                throw new Exception("Invalid file: ${videoFile.canonicalPath}")
            }
        }

        public void runCommand(File videoFile, String outputFilePrefix) {
            String command = getTrimCommand(videoFile, outputFilePrefix)
            println command
            Process process = command.execute((List) null, new File('.'))
            process.consumeProcessOutput(System.out, System.err)
            process.waitFor()
        }
    }

    private static List<VideoSegment> parseTimestamps(File timestampFile) {
        String[] lines = timestampFile.readLines()
        DateFormat outputFormat = new SimpleDateFormat('yyyyMMddHHmmss')
        Matcher startMatcher = lines.first() =~ /^Start:\s([\d]{14})$/
        Date startTime = outputFormat.parse((String) startMatcher[0][1])

        return lines.tail().collect { String line ->
            String lineRegex = /^[\w]+[.]([\w]+)[(][)]:\s([\d]{14})\s([\d]{14})$/
            if(!(line ==~ lineRegex)) {
                println "Error parsing video timestamp: ${line}"
                return null
            }

            Matcher lineMatcher = line =~ lineRegex
            String methodName = (String) lineMatcher[0][1]
            Date beginTime = outputFormat.parse((String) lineMatcher[0][2])
            Date endTime = outputFormat.parse((String) lineMatcher[0][3])
            return new VideoSegment(methodName, Math.max(0, (int) (TimeCategory.minus(beginTime, startTime).toMilliseconds() / 1000) - 10), (int) (TimeCategory.minus(endTime, startTime).toMilliseconds() / 1000))
        }.findAll { it != null }
    }

    public static void main(String[] args) {
        File videoFile = new File(args[0])
        File timestampFile = new File(args[1])
        String outputFilePrefix = args[2]

        List<VideoSegment> segments = parseTimestamps(timestampFile)
        segments*.runCommand(videoFile, outputFilePrefix)
    }
}
